// es5 classes

var Pessoa = function (nome) {
    this.nome = nome;
    this.identidade = 0;
    this.cpf = 0;

    this.getNome = function () {
        return this.nome;
    }

    this.setNome = function (nome) {
        this.nome = nome;
    }
    // ...
}