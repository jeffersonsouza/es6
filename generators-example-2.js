function* sum(number) {
    yield (number + 1);
    yield (number + 2);
    yield (number + 3);
    yield (number + 4);
    return number;
}

let calc = sum(5);
console.log(calc.next()); // ​​​​​{ value: 6, done: false }​​​​​
console.log(calc.next()); // ​​​​​{ value: 7, done: false }​​​​​
console.log(calc.next()); // ​​​​​{ value: 8, done: false }​​​​​
console.log(calc.next()); // ​​​​​{ value: 9, done: false }​​​​​
console.log(calc.next()); // ​​​​​{ value: 5, done: true }​​​​​