for (var i = 0; i < 10; i++) { /* some code */ }
for (let j = 0; j < 10; j++) { /* some code */}

console.log(i); // 10
//console.log(j); // Uncaught ReferenceError: j is not defined

// Constants
const NAME = 'Nasajon Sistemas';
const COMPANY = {
    name: 'Nasajon',
    age: 34,
    city: 'Rio de Janeiro'
};

COMPANY.age++;

COMPANY = 'Nasajon Sistemas'; // ​​Assignment to constant variable.​​
console.log(COMPANY); //{ name: 'Nasajon', age: 35, city: 'Rio de Janeiro' }​​​​​