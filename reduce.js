let users = [
    { name: 'Bruno Chagas', logins: 58},
    { name: 'Jefferson Santos', logins: 45},
    { name: 'Rodrigo Dirk', logins: 63}
];

let total = users.reduce((totalVal, user) => {
    return totalVal + user.logins;
}, 0);

console.log(total); // 166