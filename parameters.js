const sum = function (first, ...more) {
    let total = 0;
    more.forEach((number) => {
        total += number;
    });
    return total;
}