// Utilizando o var
function es5() {
    var name = 'Nasajon'
    if (true) {
        var surname = 'Sistemas';
    }

    console.log(name);      // Nasajon
    console.log(surname);   // Sistemas
}

// utilizando let
function es6() {
    let name = 'Nasajon';
    if (true) {
        let surname = 'Sistemas';
    }

    console.log(name);      // Nasajon
    console.log(surname);   // Uncaught ReferenceError: surname is not defined
}

