let names = [
    'Jefferson Santos',
    'Bruno Chagas',
    'Rodrigo Dirk'
];

let slugs = names.map((name) => {
    return name.toLowerCase().replace(/ /g, '');
});

console.log(slugs); // [ 'jeffersonsantos', 'brunochagas', 'rodrigodirk' ]​​​​​