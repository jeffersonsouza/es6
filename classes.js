// es6 classes

class Pessoa {
    constructor(nome) {
        this._nome = nome;
    }

    get nome() {
        return this._nome;
    }

    set nome(nome) {
        this._nome = nome;
    }

    falar(texto) {
        return texto;
    }
}

const jeff = new Pessoa('Jeff');
jeff.nome(); // Jeff

class Employer extends Pessoa {
    constructor(nome, identidadenasajon) {
        this._nome = nome;
        this._identidadenasajon = identidadenasajon;
    }

    get identidadenasajon() {
        return this._nome;
    }

    set identidadenasajon(identidadenasajon) {
        this._identidadenasajon = identidadenasajon;
    }
}

const employer = new Employer('Jeff', 'jeffersonsantos@nasajon.com.br');