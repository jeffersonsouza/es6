let multiply = function (number, base = 1) {
    return number * base;
}

multiply(10, 5);
multiply(38)

// pode ser declara desta forma
let multiply = (number, base = 1) => {
    return number * base;
}

// ou mais simplificada, como neste exemplo
let multiply = (number, base = 1) => number * base;

multiply(10, 5);
multiply(38)

// Exemplo de função problemática no ES5
function save() {
    let el = document.getElementById('meuBotao');

    el.addEventListener('click', function () {
        this.doHttpCall(); // Irá retornar undefined, pois dentro deste escopo nõa existe este método
    });
}

// ES6
function save() {
    let el = document.getElementById('meuBotao');

    el.addEventListener('click', () => {
        this.doHttpCall(); // Irá executar a função doHttpCall, existente na sua classe/objeto
    });
}